/*
NOTE: Do not change the name of this file

NOTE: For all file operations use promises with fs. Promisify the fs callbacks rather than use fs/promises.

Q1. Create 2 files simultaneously (without chaining).
Wait for 2 seconds and starts deleting them one after another. (in order)
(Finish deleting all the files no matter what happens with the creation code. Ensure that this is tested well)


Q2. Create a new file with lipsum data (you can google and get this). 
Do File Read and write data to another file
Delete the original file 
Using promise chaining
*/


// function login(user, val) {
//     if (val % 2 === 0) {
//         return Promise.resolve(user);
//     } else {
//         return Promise.reject(new Error("User not found"));
//     }
// }

// function getData() {
//     return Promise.resolve([
//         {
//             id: 1,
//             name: "Test",
//         },
//         {
//             id: 2,
//             name: "Test 2",
//         }
//     ]);
// }


// function logData(user, activity) {
//     // use promises and fs to save activity in some file
// }

/*
Q3.
Use appropriate methods to 
A. login with value 3 and call getData once login is successful
B. Write logic to logData after each activity for each user. Following are the list of activities
    "Login Success"
    "Login Failure"
    "GetData Success"
    "GetData Failure"

    Call log data function after each activity to log that activity in the file.
    All logged activity must also include the timestamp for that activity.
    
    You may use any data you want as the `user` object as long as it represents a valid user (eg: For you, for your friend, etc)
    All calls must be chained unless mentioned otherwise.
    
*/


const path = require('path')
const fs = require('fs')
const { resolve } = require('path')


/* Q1. Create 2 files simultaneously (without chaining).
Wait for 2 seconds and starts deleting them one after another. (in order)
(Finish deleting all the files no matter what happens with the creation code. Ensure that this is tested well)
*/


// function createFiles() {
//     // let fileCount = new Array(2)
//     let indexArray = Array.from({ length: 2 }, (_, v) => v)

//     let filesArray = indexArray.map(element => {
//         return new Promise((resolve, reject) => {

//             fs.writeFile(path.join(__dirname, `${element + 1}_file.txt`),
//                 'some random text',
//                 'utf-8', function (err) {
//                     if (err) {
//                         reject(err)
//                     } else {
//                         resolve(element)
//                     }
//                 })
//         })
//     })
//     return Promise.all(filesArray)
// }

// function deleteFiles(pr) {

//     let filesArray = pr.map(element => {
//         return new Promise((resolve, reject) => {
//             setTimeout(() => {
//                 fs.unlink(path.join(__dirname, `${element + 1}_file.txt`)
//                     , function (err) {
//                         if (err) {
//                             console.error(err)
//                         } else {
//                             resolve(element)
//                         }
//                     })

//             }, 2 * 1000)
//         })
//     })
//     return Promise.all(filesArray)

// }

// createFiles()
//     .then((pr) => {
//         console.log('Files have been created\n')
//         return deleteFiles(pr)
//     })
//     .then(() => {
//         console.log('Files have been deleted')
//     })
//     .catch(err => {
//         console.error(err)
//     })


/*
Q2. Create a new file with lipsum data (you can google and get this). 
Do File Read and write data to another file
Delete the original file 
Using promise chaining
*/

function createLipsum(text) {
    return new Promise((resolve, reject) => {
        fs.writeFile(path.join(__dirname, 'lipsum.txt'), text, 'utf-8', function (err) {
            if (err) {
                reject(err)

            } else {
                resolve('lipsum.txt')
            }
        })
    })
}
function readLipsum(text) {
    return new Promise((resolve, reject) => {
        fs.readFile(path.join(__dirname, 'lipsum.txt'), 'utf-8', function (err, data) {
            if (err) {
                reject(err)

            } else {
                resolve(data)
            }
        })

    })
}
function writeToFile(text) {
    return new Promise((resolve, reject) => {
        fs.writeFile(path.join(__dirname, 'lipsum(1).txt'), text, 'utf-8', function (err) {
            if (err) {
                reject(err)

            } else {
                resolve('lipsum(1).txt')
            }
        })
    })
}
function deleteLipsum(file) {
    return new Promise((resolve, reject) => {
        fs.unlink(path.join(__dirname, file), function (err) {
            if (err) {
                reject(err)
            } else {
                resolve('Done')
            }
        })
    })
}

let text = `Lorem ipsum dolor sit amet consectetur adipisicing elit. Maxime mollitia,
molestiae quas vel sint commodi repudiandae consequuntur voluptatum laborum
numquam blanditiis harum quisquam eius sed odit fugiat iusto fuga praesentium
optio, eaque rerum! Provident similique accusantium nemo autem. Veritatis
obcaecati tenetur iure eius earum ut molestias architecto voluptate aliquam
nihil, eveniet aliquid culpa officia aut! Impedit sit sunt quaerat, odit,
tenetur error, harum nesciunt ipsum debitis quas aliquid. Reprehenderit,
quia. Quo neque error repudiandae fuga? Ipsa laudantium molestias eos 
sapiente officiis modi at sunt excepturi expedita sint? Sed quibusdam
recusandae alias error harum maxime adipisci amet laborum. Perspiciatis 
minima nesciunt dolorem! Officiis iure rerum voluptates a cumque velit 
quibusdam sed amet tempora. Sit laborum ab, eius fugit doloribus tenetur 
fugiat, temporibus enim commodi iusto libero magni deleniti quod quam 
consequuntur! Commodi minima excepturi repudiandae velit hic maxime
doloremque. Quaerat provident commodi consectetur veniam similique ad 
earum omnis ipsum saepe, voluptas, hic voluptates pariatur est explicabo 
fugiat, dolorum eligendi quam cupiditate excepturi mollitia maiores labore 
suscipit quas? Nulla, placeat. Voluptatem quaerat non architecto ab laudantium
modi minima sunt esse temporibus sint culpa, recusandae aliquam numquam 
totam ratione voluptas quod exercitationem fuga. Possimus quis earum veniam 
quasi aliquam eligendi, placeat qui corporis!`

createLipsum(text)
    .then((file) => {
        console.log("Lipsum file has been created")
        return readLipsum(file)
    })
    .then((text) => {
        console.log('File has been read')
        return writeToFile(text)
    })
    .then(() => {
        console.log('File has been copied to lipsum(1).txt')
        return deleteLipsum('lipsum.txt')
    })
    .then(() => {
        console.log("All done")
    })

/*
Q3. Use appropriate methods to
A. login with value 3 and call getData once login is successful
B. Write logic to logData after each activity for each user. Following are the list of activities
    "Login Success"
    "Login Failure"
    "GetData Success"
    "GetData Failure"

    Call log data function after each activity to log that activity in the file.
    All logged activity must also include the timestamp for that activity.

    You may use any data you want as the `user` object as long as it represents a valid user (eg: For you, for your friend, etc)
    All calls must be chained unless mentioned otherwise.
*/


// function login(user, val) {
//     if (val % 2 === 0) {
//         return Promise.resolve(user);
//     } else {
//         return Promise.reject(new Error("User not found"));
//     }
// }

// function getData() {
//     return Promise.resolve([
//         {
//             id: 1,
//             name: "Test",
//         },
//         {
//             id: 2,
//             name: "Test 2",
//         }
//     ]);
// }
