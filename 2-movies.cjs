const favouritesMovies = {
    "Matrix": {
        imdbRating: 8.3,
        actors: ["Keanu Reeves", "Carrie-Anniee"],
        oscarNominations: 2,
        genre: ["sci-fi", "adventure"],
        totalEarnings: "$680M"
    },
    "FightClub": {
        imdbRating: 8.8,
        actors: ["Edward Norton", "Brad Pitt"],
        oscarNominations: 6,
        genre: ["thriller", "drama"],
        totalEarnings: "$350M"
    },
    "Inception": {
        imdbRating: 8.3,
        actors: ["Tom Hardy", "Leonardo Dicaprio"],
        oscarNominations: 12,
        genre: ["sci-fi", "adventure"],
        totalEarnings: "$870M"
    },
    "The Dark Knight": {
        imdbRating: 8.9,
        actors: ["Christian Bale", "Heath Ledger"],
        oscarNominations: 12,
        genre: ["thriller"],
        totalEarnings: "$744M"
    },
    "Pulp Fiction": {
        imdbRating: 8.3,
        actors: ["Sameul L. Jackson", "Bruce Willis"],
        oscarNominations: 7,
        genre: ["drama", "crime"],
        totalEarnings: "$455M"
    },
    "Titanic": {
        imdbRating: 8.3,
        actors: ["Leonardo Dicaprio", "Kate Winslet"],
        oscarNominations: 13,
        genre: ["drama"],
        totalEarnings: "$800M"
    }
}


/*
    NOTE: For all questions, the returned data must contain all the movie information including its name.

    Q1. Find all the movies with total earnings more than $500M. 
    Q2. Find all the movies who got more than 3 oscarNominations and also totalEarning are more than $500M.
    Q.3 Find all movies of the actor "Leonardo Dicaprio".
    Q.4 Sort movies (based on IMDB rating)
        if IMDB ratings are same, compare totalEarning as the secondary metric.
    Q.5 Group movies based on genre. Priority of genres in case of multiple genres present are:
        drama > sci-fi > adventure > thriller > crime

    NOTE: Do not change the name of this file
*/ 


// Q1. Find all the movies with total earnings more than $500M. 

function earnings(movies){
    movies = Object.entries(movies)

    let result = movies.filter( (movie) => {
        let earning = parseInt(movie[1].totalEarnings.replace("$",""))
        if (earning > 500){
            return true
        }
        else{
            return false
        }

    })
    return Object.fromEntries(result)
}

console.log("All the movies with total earnings more than $500M: \n" ,earnings(favouritesMovies), "\n")


// Q2. Find all the movies who got more than 3 oscarNominations and also totalEarning are more than $500M.

function oscar(movies){
    movies = Object.entries(movies)
    
    let result = movies.filter ( (movie) => {
        let nominations = movie[1].oscarNominations
        if (nominations > 3){
            return true
        }else{
            return false
        }
        })
    
    return Object.fromEntries(result)

}

console.log("All the movies who got more than 3 oscarNominations and also totalEarning are more than $500M: \n",oscar(earnings(favouritesMovies)),"\n")


// 3. Find all movies of the actor "Leonardo Dicaprio".

function leonardo(movies){
    movies = Object.entries(movies)

    let result = movies.filter( (movie) => {
        let actors = movie[1].actors
        if (actors.includes("Leonardo Dicaprio")){
            return true
        }
        else{
            return false
        }
    })
    return Object.fromEntries(result)
}

console.log("All movies of the actor Leonardo Dicaprio: ",leonardo(favouritesMovies),"\n")


// Q.4 Sort movies (based on IMDB rating)

function sort(movies){
    movies = Object.entries(movies)

    let result = movies.map( movie => movie).sort( (movieA, movieB) => {
        // console.log(movieA)
        let idmbA = movieA[1].imdbRating
        let idmbB = movieB[1].imdbRating
        
        if (idmbA > idmbB){
            return -1

        }else if (idmbA < idmbB){
            return 1

        }else{
            let earningA = parseInt(movieA[1].totalEarnings.replace("$",""))
            let earningB = parseInt(movieB[1].totalEarnings.replace("$",""))
            if (earningA > earningB){
                return -1
            }
            else if( earningA < earningB){
                return 1
            }
            else{
                return 0
            }
        }
        
    })
    return Object.fromEntries(result)
}


console.log("Sort movies (based on IMDB rating): ", sort(favouritesMovies),"\n")


// Q5. Group movies based on genre. Priority of genres in case of multiple genres present are:
// drama > sci-fi > adventure > thriller > crime

function group(movies){
    movies = Object.entries(movies)

    const genres = ["drama", "sci-fi", "adventure", "thriller", "crime"]
    // let mainGenre = movies.map( movie => {
    //     let genres = movie[1].genre
    //     console.log(genres)



    // })

    let result = genres.map( movie => movie)
                            .reduce( (acc1, ele1) => {
                                // console.log(ele1)
                                if (acc1[ele1] === undefined){
                                    acc1[ele1] = movies.reduce( (acc2, ele2) => {
                                        if (ele2[1].genre.includes(ele1)){
                                            acc2.push(ele2[0])
                                        }
                                        return acc2
                                    }, [])
                                    
                                }
                                return acc1
                            },{})
    return result



}

console.log("Group movies based on genre: ",group(favouritesMovies))