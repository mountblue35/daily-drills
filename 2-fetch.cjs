
/*
NOTE: Do not change the name of this file

* Ensure that error handling is well tested.
* If there is an error at any point, the subsequent solutions must not get executed.
* Solutions without error handling will get rejected and you will be marked as having not completed this drill.

* Usage of async and await is not allowed.

Users API url: https://jsonplaceholder.typicode.com/users
Todos API url: https://jsonplaceholder.typicode.com/todos

Users API url for specific user ids : https://jsonplaceholder.typicode.com/users?id=2302913
Todos API url for specific user Ids : https://jsonplaceholder.typicode.com/todos?userId=2321392

Using promises and the `fetch` library, do the following. 

1. Fetch all the users
2. Fetch all the todos
3. Use the promise chain and fetch the users first and then the todos.
4. Use the promise chain and fetch the users first and then all the details for each user.
5. Use the promise chain and fetch the first todo. Then find all the details for the user that is associated with that todo

NOTE: If you need to install `node-fetch` or a similar libary, let your mentor know before doing so along with the reason why. No other exteral libraries are allowed to be used.

Usage of the path libary is recommended


*/


const path = require('path')

// 1. Fetch all the users

// fetch('https://jsonplaceholder.typicode.com/users')
//     .then(function (response) {
//         return response.json()
//     })
//     .then(function (users) {
//         console.log(users)
//     })
//     .catch(function (err) {
//         console.error(err)

//     })



// 2. Fetch all the todos

// fetch('https://jsonplaceholder.typicode.com/todos')
// .then(function (response) {
//     return response.json()
// }).then(function (todos) {
//     console.log(todos)
// }).catch(function (err) {
//     console.error("Error",err)
// })



// 3. Use the promise chain and fetch the users first and then the todos.

// fetch('https://jsonplaceholder.typicode.com/users')
//     .then(function (response) {
//         return response.json()
//     })
//     .then(function (users) {
//         console.log(users)
//         return fetch('https://jsonplaceholder.typicode.com/todos')
//     })
//     .then(function (response) {
//         return response.json()
//     })
//     .then(function (todos) {
//         console.log(todos)
//     })
//     .catch(function (err) {
//         console.error(err)
//     })



// 4. Use the promise chain and fetch the users first and then all the details for each user.

// fetch('https://jsonplaceholder.typicode.com/users')
//     .then(function (response) {
//         return response.json()
//     })
//     .then(function (users) {
//         let userArray = users.map(user => {
//             let id = user.id
//             return fetch(`https://jsonplaceholder.typicode.com/users?id=${id}`).then((res)=>{
//                 return res.json()
//             })                
//         })
//         return Promise.all(userArray)
//     })
//     .then(function (response) {
//         console.log(response)
//     })
//     .catch(function (err) {
//         console.error(err)
//     })


// 5. Use the promise chain and fetch the first todo. Then find all the details for the user that is associated with that todo


fetch(`https://jsonplaceholder.typicode.com/todos`)
    .then(function (response) {
        return response.json()
    })
    .then(function (todo) {
        let first = todo[0].userId
        return fetch(`https://jsonplaceholder.typicode.com/users?id=${first}`)
    })
    .then(function (response) {
        return response.json()
    })
    .then(function (user) {
        console.log(user)
    })
    .catch(function (err) {
        console.error(err)
    })