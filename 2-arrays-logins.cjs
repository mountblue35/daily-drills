const input = [{"id":1,"first_name":"Valera","last_name":"Pinsent","email":"vpinsent0@google.co.jp","gender":"Male","ip_address":"253.171.63.171"},
{"id":2,"first_name":"Kenneth","last_name":"Hinemoor","email":"khinemoor1@yellowbook.com","gender":"Polygender","ip_address":"50.231.58.150"},
{"id":3,"first_name":"Roman","last_name":"Sedcole","email":"rsedcole2@addtoany.com","gender":"Genderqueer","ip_address":"236.52.184.83"},
{"id":4,"first_name":"Lind","last_name":"Ladyman","email":"lladyman3@wordpress.org","gender":"Male","ip_address":"118.12.213.144"},
{"id":5,"first_name":"Jocelyne","last_name":"Casse","email":"jcasse4@ehow.com","gender":"Agender","ip_address":"176.202.254.113"},
{"id":6,"first_name":"Stafford","last_name":"Dandy","email":"sdandy5@exblog.jp","gender":"Female","ip_address":"111.139.161.143"},
{"id":7,"first_name":"Jeramey","last_name":"Sweetsur","email":"jsweetsur6@youtube.com","gender":"Genderqueer","ip_address":"196.247.246.106"},
{"id":8,"first_name":"Anna-diane","last_name":"Wingar","email":"awingar7@auda.org.au","gender":"Agender","ip_address":"148.229.65.98"},
{"id":9,"first_name":"Cherianne","last_name":"Rantoul","email":"crantoul8@craigslist.org","gender":"Genderfluid","ip_address":"141.40.134.234"},
{"id":10,"first_name":"Nico","last_name":"Dunstall","email":"ndunstall9@technorati.com","gender":"Female","ip_address":"37.12.213.144"}]

/* 

    1. Find all people who are Agender
    2. Split their IP address into their components eg. 111.139.161.143 has components [111, 139, 161, 143]
    3. Find the sum of all the second components of the ip addresses.
    3. Find the sum of all the fourth components of the ip addresses.
    4. Compute the full name of each person and store it in a new key (full_name or something) for each person.
    5. Filter out all the .org emails
    6. Calculate how many .org, .au, .com emails are there
    7. Sort the data in descending order of first name

    NOTE: Do not change the name of this file

*/

// 1. Find all people who are Agender

function agender(input){
    let result = input.filter((ele, index) => {
        if (ele.gender === "Agender"){
            return true
        }
    })
    return result    
}

// console.log(agender(input))


// 2. Split their IP address into their components eg. 111.139.161.143 has components [111, 139, 161, 143]

function ipAddress(input){
    
    let result = []
    result = input
            .map( (ip) => {
                let ip_log = ip.ip_address
                let ip_digits = ip_log.split(".").map( x => parseInt(x)) 
                return ip_digits
            })
    
    return result
}

// console.log(ipAddress(input))

// 3. Find the sum of all the second components of the ip addresses.

function sumIp(input, comp){
    let result = 0

    result = input.map(value => value[comp])
    .reduce ( (acc, ele) => {
        return acc + ele
    },0)
    return (result)
}

// console.log(sumIp(ipAddress(input), 1))    


// 4. Find the sum of all the fourth components of the ip addresses.

// console.log(sumIp(ipAddress(input), 3))

// 5. Compute the full name of each person and store it in a new key (full_name or something) for each person.

function fullName(input){
    input.forEach((value) => value.full_name = value.first_name +" "+ value.last_name)
    return input
}

// console.log(fullName(input))

// 6. Filter out all the .org emails

function orgMail(input){
    let org_mails = input.filter(value => value.email.includes(".org"))
                        .map(value => value.email)
    return org_mails
}

// console.log(orgMail(input))

// 7. Calculate how many .org, .au, .com emails are there

function specialEmails(input){
    
    const result = input.filter(value => value.email.includes(".org") || value.email.includes(".au") || value.email.includes(".com"))
    .map( value => value.email)
    .reduce( (acc, element) => {
        if (element.includes('org')){
            if (acc.hasOwnProperty('org')){
                acc['org']++
            }else{
                acc['org'] = 1 
            }
        }
        if (element.includes('au')){
            if (acc.hasOwnProperty('au')){
                acc['au']++
            }else{
                acc['au'] = 1 
            }
        }
        if (element.includes('com')){
            if (acc.hasOwnProperty('com')){
                acc['com']++
            }else{
                acc['com'] = 1 
            }
        }
        return acc
    }, {} )
    return result
}

// console.log(specialEmails(input))

// 8. Sort the data in descending order of first name

function order(input){
    let result = []
    result = input.sort( ( person_a, person_b) => {
        const a = person_a.first_name
        const b = person_b.first_name
        if (a > b){
            return -1
        }
        else{
            return 1
        } 
    } )
    
    return result
}

// console.log(order(input))
