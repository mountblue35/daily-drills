const products = [{
    shampoo: {
        price: "$50",
        quantity: 4
    },
    "Hair-oil": {
        price: "$40",
        quantity: 2,
        sealed: true
    },
    comb: {
        price: "$12",
        quantity: 1
    },
    utensils: [
        {
            spoons: { quantity: 2, price: "$8" }
        }, {
            glasses: { quantity: 1, price: "$70", type: "fragile" }
        }, {
            cooker: { quantity: 4, price: "$900" }
        }
    ],
    watch: {
        price: "$800",
        quantity: 1,
        type: "fragile"
    }
}]


/*

Q1. Find all the items with price more than $65.
Q2. Find all the items where quantity ordered is more than 1.
Q.3 Get all items which are mentioned as fragile.
Q.4 Find the least and the most expensive item for a single quantity.
Q.5 Group items based on their state of matter at room temperature (Solid, Liquid Gas)

NOTE: Do not change the name of this file

*/



// Q1. Find all the items with price more than $65.

function price(products) {
    let pairs = products[0]
    let result = []
    Object.entries(pairs)
        .map(pair => pair)
        .forEach((product) => {
            // console.log(product)

            if (Array.isArray(product[1])) {

                product = product[1].map(items => {
                    items = Object.entries(items)
                    // console.log(items)
                    let price = Number(items[0][1].price.replaceAll("$", ""))

                    items.push(price)
                    return items

                }).forEach(items => {
                    if (items[1] > 65) {
                        delete items[2]
                        result.push(items)
                    }
                })

            } else {
                let price = Number(product[1].price.replaceAll("$", ""))

                if (price > 65) {
                    // console.log(price, typeof price)
                    result.push(product)
                }

            }

        })

    return result

}


console.log(price(products))