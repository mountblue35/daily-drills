const users = {
    "John": {
        age: 24,
        desgination: "Senior Golang Developer",
        interests: ["Chess, Reading Comics, Playing Video Games"],
        qualification: "Masters",
        nationality: "Greenland"
    },
    "Ron": {
        age: 19,
        desgination: "Intern - Golang",
        interests: ["Video Games"],
        qualification: "Bachelor",
        nationality: "UK"
    },
    "Wanda": {
        age: 24,
        desgination: "Intern - Javascript",
        interests: ["Piano"],
        qualification: "Bachaelor",
        nationality: "Germany"
    },
    "Rob": {
        age: 34,
        desgination: "Senior Javascript Developer",
        interests: ["Walking his dog, Cooking"],
        qualification: "Masters",
        nationality: "USA"
    },
    "Pike": {
        age: 23,
        desgination: "Python Developer",
        interests: ["Listing Songs, Watching Movies"],
        qualification: "Bachaelor's Degree",
        nationality: "Germany"
    }
}


/*

Q1 Find all users who are interested in playing video games.
Q2 Find all users staying in Germany.
Q3 Sort users based on their seniority level 
   for Designation - Senior Developer > Developer > Intern
   for Age - 20 > 10
Q4 Find all users with masters Degree.
Q5 Group users based on their Programming language mentioned in their desig1nation.

NOTE: Do not change the name of this file

*/ 


// Q1 Find all users who are interested in playing video games.

function videoGames(users){
    users = Object.entries(users)

    let result = users.filter( (user) => {
        let interestsList = user[1].interests
        let interests = interestsList[0].split(",")
        
        if (interests.find( (value) => {
            if (value.includes("Video Games")){
                return true
            }
        })){
            return true
        }else{

            return false
        }
    })
    return Object.fromEntries(result)
}

console.log("Q1. All users who are interested in playing video games: ",videoGames(users),"\n")


// Q2 Find all users staying in Germany.

function nation(users){
    users = Object.entries(users)

    let result = users.filter( (user) => {
        let nationality = user[1].nationality

        if (nationality === "Germany"){
            return true
        }else{
            return false
        }
    })
    return Object.fromEntries(result)
}

console.log("Q2. All users staying in Germany: ",nation(users),"\n")


// Q3 Sort users based on their seniority level 

function sort(users){
    users = Object.entries(users)

    let designationOrderArray = users.map( (value) => {
        let designation = value[1].desgination
        let rank  = 1

        if (designation.includes("Senior") && designation.includes("Developer")){
            value[1].rank = 3
        }else if(designation.includes("Developer")){
            value[1].rank = 2
        }else if(designation.includes("Intern")){
            value[1].rank = 1 
        } 
        return value
    })
    console.log(designationOrderArray)

    let result = users.sort( (a, b) => {
        // console.log(a, b)

        const aRank = a[1].rank
        const bRank = b[1].rank

        const aAge = a[1].age
        const bAge = b[1].age

        if (aRank > bRank){
            return -1
        }else if ( aRank < bRank){
            return 1
        }else if (aRank == bRank){
            if (aAge > bAge){
                return -1
            }else{
                return 1
            }
        }
    }) 

    users.map(value => {
        delete value[1].rank
    })

    return Object.fromEntries(result)

}

console.log("Q3. Sort users based on their seniority level: ",sort(users),"\n")

// Q4 Find all users with masters Degree.

function degree(users){
    users = Object.entries(users)
    let result = users.filter( (value) => {
        let degree = value[1].qualification

        if (degree === "Masters"){
            return true
        }else{
            return false
        }
    })
    return Object.fromEntries(result)
}

console.log("Q4. All users with masters Degree: ",degree(users),"\n")


// Q5 Group users based on their Programming language mentioned in their designation.

function group(users){
    const hashtable = [["Javascript"],["Python"],["Golang"]] 
    users = Object.entries(users)
    const languages = users.map( (language) => language[1].desgination.split(" ").flat(1))
    // console.log(languages)
    let result = hashtable.map( value => value)
                            .reduce( (acc1, ele1) => {
                                if (acc1[ele1] === undefined){
                                    acc1[ele1] = users.reduce( (acc2, ele2) => {
                                        if (ele2[1].desgination.includes(ele1)){
                                            acc2.push(ele2[0])
                                        }
                                        return acc2
                                    }, [])
                                    
                                }
                                return acc1
                            },{})
    return result
}

console.log("Q5. Group users based on their Programming language ",group(users),"\n")