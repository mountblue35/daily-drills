/*
 * Use asynchronous callbacks ONLY wherever possible.
 * Error first callbacks must be used always.
 * Each question's output has to be stored in a json file.
 * Each output file has to be separate.

 * Ensure that error handling is well tested.
 * After one question is solved, only then must the next one be executed. 
 * If there is an error at any point, the subsequent solutions must not get executed.
   
 * Store the given data into data.json
 * Read the data from data.json
 * Perfom the following operations.

    1. Retrieve data for ids : [2, 13, 23].
    2. Group data based on companies.
        { "Scooby Doo": [], "Powerpuff Brigade": [], "X-Men": []}
    3. Get all data for company Powerpuff Brigade
    4. Remove entry with id 2.
    5. Sort data based on company name. If the company name is same, use id as the secondary sort metric.
    6. Swap position of companies with id 93 and id 92.
    7. For every employee whose id is even, add the birthday to their information. The birthday can be the current date found using `Date`.

    NOTE: Do not change the name of this file

*/

// {
//     "employees": [
//         {
//             "id": 23,
//             "name": "Daphny",
//             "company": "Scooby Doo"
//         },
//         {
//             "id": 73,
//             "name": "Buttercup",
//             "company": "Powerpuff Brigade"
//         },
//         {
//             "id": 93,
//             "name": "Blossom",
//             "company": "Powerpuff Brigade"
//         },
//         {
//             "id": 13,
//             "name": "Fred",
//             "company": "Scooby Doo"
//         },
//         {
//             "id": 89,
//             "name": "Welma",
//             "company": "Scooby Doo"
//         },
//         {
//             "id": 92,
//             "name": "Charles Xavier",
//             "company": "X-Men"
//         },
//         {
//             "id": 94,
//             "name": "Bubbles",
//             "company": "Powerpuff Brigade"
//         },
//         {
//             "id": 2,
//             "name": "Xyclops",
//             "company": "X-Men"
//         }
//     ]
// }


const path = require("path")
const fs = require("fs")


// 1. Retrieve data for ids : [2, 13, 23].

function problem() {
    const dir = __dirname
    const dataFile = path.join(dir, 'data.json')
    fs.readFile(dataFile, "utf-8", (err, data) => {
        if (err) {
            console.error(err)
        } else {
            data = Object.entries(JSON.parse(data))[0][1]
            // console.log(data)
            let result1 = data.filter(employee => {
                let id = employee.id
                if (id == 23 || id == 13 || id == 2) {
                    return true
                }

            })
            fs.writeFile(path.join(dir, 'output1.json'), JSON.stringify(result1), "utf-8", function (err) {
                if (err) {
                    console.error(err)
                } else {
                    let result2 = data.reduce((accu, employee) => {
                        let company = employee.company
                        if (accu.hasOwnProperty(company)) {
                            accu[company].push(employee)
                        } else {
                            accu[company] = []
                            accu[company].push(employee)
                        }
                        return accu

                    }, {})
                    fs.writeFile(path.join(dir, 'output2.json'), JSON.stringify(result2), "utf-8", function (err) {
                        if (err) {
                            console.error(err)
                        } else {
                            let result3 = result2['Powerpuff Brigade']
                            fs.writeFile(path.join(dir, 'output3.json'), JSON.stringify(result3), "utf-8", function (err) {
                                if (err) {
                                    console.error(err)
                                } else {
                                    let result4 = data.filter((employee) => {
                                        if (employee.id == 2) {
                                            return false
                                        } else {
                                            return true
                                        }
                                    })
                                    fs.writeFile(path.join(dir, 'output4.json'), JSON.stringify(result4), "utf-8", function (err) {
                                        if (err) {
                                            console.error(err)
                                        } else {
                                            let result5 = data.sort((employeeA, employeeB) => {
                                                let companyA = employeeA.company
                                                let companyB = employeeB.company
                                                let idA = employeeA.id
                                                let idB = employeeB.id

                                                if (companyA > companyB) {
                                                    return 1
                                                } else if (companyA < companyB) {
                                                    return -1
                                                } else {
                                                    return idA - idB
                                                }
                                            })
                                            fs.writeFile(path.join(dir, 'output5.json'), JSON.stringify(result5), "utf-8", function (err) {
                                                if (err) {
                                                    console.error(err)
                                                } else {
                                                    let index93 = data.findIndex(employee => {
                                                        if (employee.id == 93) {
                                                            return true
                                                        }

                                                    })
                                                    let index92 = data.findIndex(employee => {
                                                        if (employee.id == 92) {
                                                            return true
                                                        }
                                                    })
                                                    let result6 = data
                                                    let temp = result6[index93]
                                                    result6[index93] = result6[index92]
                                                    result6[index92] = temp

                                                    fs.writeFile(path.join(dir, 'output6.json'), JSON.stringify(result6), "utf-8", function (err) {
                                                        if (err) {
                                                            console.error(err)
                                                        } else {
                                                            let result7 = data.map((employee, index) => {
                                                                let id = employee.id

                                                                if (id % 2 == 0) {

                                                                    let date = new Date().toString()
                                                                    employee['birthday'] = date.slice(4, 15)
                                                                }

                                                                return employee

                                                            })
                                                            fs.writeFile(path.join(dir, 'output7.json'), JSON.stringify(result7), "utf-8", function (err) {
                                                                if (err) {
                                                                    console.error(err)

                                                                } else {
                                                                    console.log("Done without errors")
                                                                }
                                                            })

                                                        }
                                                    })
                                                }
                                            })
                                        }
                                    })
                                }
                            })
                        }
                    })

                }
            })


        }

    })
}

problem()