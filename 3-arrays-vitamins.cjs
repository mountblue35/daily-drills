const items = [{
    name: 'Orange',
    available: true,
    contains: "Vitamin C",
}, {
    name: 'Mango',
    available: true,
    contains: "Vitamin K, Vitamin C",
}, {
    name: 'Pineapple',
    available: true,
    contains: "Vitamin A",
}, {
    name: 'Raspberry',
    available: false,
    contains: "Vitamin B, Vitamin A",

}, {
    name: 'Grapes',
    contains: "Vitamin D",
    available: false,
}];


/*

    1. Get all items that are available 
    2. Get all items containing only Vitamin C.
    3. Get all items containing Vitamin A.
    4. Group items based on the Vitamins that they contain in the following format:
        {
            "Vitamin C": ["Orange", "Mango"],
            "Vitamin K": ["Mango"],
        }
        
        and so on for all items and all Vitamins.
    5. Sort items based on number of Vitamins they contain.

    NOTE: Do not change the name of this file

*/ 


// 1. Get all items that are available 

function checkAvailable(items){
    let result = items.filter( (value, index) => {
        if (value.available === true){
            return value
        }
    })
    return result
}

console.log("1. All items that are available ",checkAvailable(items),"\n")

// 2. Get all items containing only Vitamin C.

function vitaminC (items){
    let result = items.filter( (value, index) => {
        let temp = value.contains
        if (temp === "Vitamin C"){
            return value
        }

    })
    return result
}

console.log("2. All items containing only Vitamin C: ",vitaminC(items),"\n")

//  3. Get all items containing Vitamin A.

function vitaminA (items){
    let result = items.filter( (value, index) => {
        let temp = value.contains
        if (temp.includes("Vitamin A")){
            return value
        }

    })
    return result
}

console.log("3. All items containing Vitamin A: ",vitaminA(items),"\n")


/* 4. Group items based on the Vitamins that they contain in the following format:
        {
            "Vitamin C": ["Orange", "Mango"],
            "Vitamin K": ["Mango"],
        }
        
        and so on for all items and all Vitamins.
*/

function group(items){
    let vitamins = items.map(value => value["contains"].split(", ")).flat(1)
    let result = {}
    // console.log(vitamins)

    result = vitamins.map( value => value)
                    .reduce( (acc1, ele1) => {
                        if (acc1[ele1] === undefined){
                            acc1[ele1] = items.reduce( (acc2,ele2) => {
                                
                                if (ele2.contains.includes(ele1)){
                                    acc2.push(ele2.name)
                                    
                                }
                                return acc2
                            }, [])
                        }
                        return acc1
                    }, {})
                    
    return result
}


console.log("4. Group items based on Vitamins they contain: ",group(items),"\n")



// 5. Sort items based on number of Vitamins they contain.

function sort(items){
    let result = []
    result = items.sort( ( first, second) => {
        const a = first["contains"].split(",").length
        const b = second["contains"].split(",").length
        if (a > b){
            return 1
        }
        else{
            return -1
        } 
    } )
    
    return result
}


console.log("5. Sort items based on number of Vitamin: ",sort(items))